
// GuitarDlg.cpp: файл реализации
//

#include "stdafx.h"
#include "Guitar.h"
#include "GuitarDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// multiplier for column number to build res id
#define RES_COL_MUL 100 

// Диалоговое окно CAboutDlg используется для описания сведений о приложении

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // поддержка DDX/DDV

// Реализация
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// Диалоговое окно CGuitarDlg



CGuitarDlg::CGuitarDlg(CWnd* pParent /*=NULL*/)
	: CDialog(IDD_GUITAR_DIALOG, pParent), m_pAccord(NULL)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CGuitarDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	
	for (int col = 0; col < NUM_COLS; col++) {
		for (int row = 0; row < NUM_ROWS; row++) {
			DDX_Control(pDX, IDC_CIRCLE_0_0+col*RES_COL_MUL+row, m_circles[col][row]);
		}
	}
}

BEGIN_MESSAGE_MAP(CGuitarDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BTN_A, &CGuitarDlg::OnBnClickedBtnA)
	ON_BN_CLICKED(IDC_BTN_B, &CGuitarDlg::OnBnClickedBtnB)
	ON_BN_CLICKED(IDC_BTN_C, &CGuitarDlg::OnBnClickedBtnC)
	ON_BN_CLICKED(IDC_BTN_D, &CGuitarDlg::OnBnClickedBtnD)
	ON_BN_CLICKED(IDC_BTN_E, &CGuitarDlg::OnBnClickedBtnE)
END_MESSAGE_MAP()


// Обработчики сообщений CGuitarDlg

BOOL CGuitarDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Добавление пункта "О программе..." в системное меню.

	// IDM_ABOUTBOX должен быть в пределах системной команды.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок

	// TODO: добавьте дополнительную инициализацию
	 
	 for (int col = 0; col < NUM_COLS; col++) {
		 for (int row = 0; row < NUM_ROWS; row++) {
			 HideCircle(col, row);
		 }
	 }

	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

void CGuitarDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void CGuitarDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR CGuitarDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CGuitarDlg::PlayAccord(CString name, UINT sound)
{
	if (m_pAccord != NULL)
	{
		CArray<CCoordinate*>& notes = m_pAccord->GetNotes();
		for (int i = 0; i < notes.GetSize(); i++)
		{
			CCoordinate* pCoordinate = notes.GetAt(i);
			HideCircle(pCoordinate->GetCol(), pCoordinate->GetRow());
		}
	}
	m_pAccord = m_model.GetAccord(name);
	if (m_pAccord == NULL) return;

	CArray<CCoordinate*>& notes = m_pAccord->GetNotes();
	for (int i = 0; i < notes.GetSize(); i++)
	{
		CCoordinate* pCoordinate = notes.GetAt(i);
		ShowCircle(pCoordinate->GetCol(), pCoordinate->GetRow());
	}
	PlaySound(
		MAKEINTRESOURCE(sound),
		GetModuleHandle(NULL),
		SND_RESOURCE | SND_ASYNC);	
}

void CGuitarDlg::ShowCircle(int col, int row)
{
	m_circles[col][row].ShowWindow(SW_SHOW);
}

void CGuitarDlg::HideCircle(int col, int row)
{
	m_circles[col][row].ShowWindow(SW_HIDE);
}

void CGuitarDlg::OnBnClickedBtnA()
{
	PlayAccord(_T("A"), IDR_WAVE_A);
}

void CGuitarDlg::OnBnClickedBtnB()
{
	PlayAccord(_T("B"), IDR_WAVE_B);
}


void CGuitarDlg::OnBnClickedBtnC()
{
	PlayAccord(_T("C"), IDR_WAVE_C);
}


void CGuitarDlg::OnBnClickedBtnD()
{
	PlayAccord(_T("D"), IDR_WAVE_D);
}


void CGuitarDlg::OnBnClickedBtnE()
{
	PlayAccord(_T("E"), IDR_WAVE_E);
}